<?php 
	function username_show($id)
	{
		require_once 'database_connection.php';
		$query_username = sprintf("SELECT username FROM register WHERE id=%d;",$id);

		$result = mysqli_query($connect_server,$query_username)
			or die(mysqli_error($connect_server));

		$row = mysqli_fetch_array($result,MYSQLI_ASSOC)
			or die(mysqli_error($connect_server));

		return $row['username'];
	}

	function neut_str($value) 
	{
		$value = htmlspecialchars(str_replace('"', '\"', trim($value)));
		return $value;
	}

	function rewrite_file($name_file) 
	{

		$open_file_messages_for_write = fopen($name_file, 'a') or 
			die("Unable to open file. No such file or dir");	

		fwrite($open_file_messages_for_write, 
			"<div class='message'>&gt;&gt;&gt;&nbsp;(".date("H:i").")&nbsp;&nbsp;<span class='username'>".username_show($_COOKIE['id'])."</span>:&nbsp;&nbsp;".neut_str($_REQUEST['text'])."</div><br>");


		fclose($open_file_messages_for_write);
		header('Location: sign_in.php?id='.$_COOKIE['id']);
		exit();
	}

	rewrite_file('messages.txt');
?>