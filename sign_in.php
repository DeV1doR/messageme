<?php
	require_once 'database_connection.php';

	function decoder($value,$salt=NULL,$connect_server) 
	{	
		$value = mysqli_real_escape_string($connect_server,trim($value));
		if (isset($salt)) {
			$dec_value = crypt($value,$salt);
			return $dec_value;
		} else {
			return $value;
		}
	}

	@$message = $_GET['message'];

	if(isset($_COOKIE['id'])) {
		$_SESSION['id'] = $_COOKIE['id'];
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		if (!empty($_POST['logout'])) 
		{	
			setcookie('id',$row['id'],time() - 3600);
			unset($_SESSION['id']);
			session_destroy();
			header('Location: sign_in.php');
			exit();
		}

		if (empty($_POST['username']) or empty($_POST['password'])) 
		{
			$message = 'Login or password field is empty';
			header('Location: sign_in.php?message='.$message);
			exit();
		}

		$username = decoder($_POST['username'],NULL,$connect_server);
		$password = decoder($_POST['password'],$username,$connect_server);

		$query_check = sprintf("SELECT id, username, password FROM register WHERE username = '%s' AND password = '%s';",
																			 	 $username,          $password) 
			or die(mysqli_error($connect_server));

		$result_check = mysqli_query($connect_server,$query_check);	
		$row = mysqli_fetch_array($result_check, MYSQLI_ASSOC);

		if ($row['username'] == $username and $row['password'] == $password) 
		{	
			session_start();
			setcookie('id',$row['id'],time() + 3600);
			$_SESSION['id'] = $_COOKIE['id'] = $row['id'];
			header('Location: sign_in.php?id='.$_COOKIE['id']);
		}

		if ($row['username'] != $username or $row['password'] != $password) 
		{
			$message = 'Login or password field is incorrect';
			header('Location: sign_in.php?message='.$message);
			exit();

		}

	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<style type="text/css">
			#text-form {
				position: fixed;
				bottom: 1%;
				left: 0.5%;
			}
			.chat {
				height: 50px;
				width: 300px;
				resize: none;
				border-radius: 7px;
			}
			.sign {
				height: 20px;
				width: 150px;
				resize: none;
				border-radius: 7px;
			}
			input.button {
				border-color: rgb(212,75,56);
				width: 150px;
  				color: #fff; 
  				text-decoration: none; 
  				user-select: none; 
  				background: rgb(212,75,56); 
  				padding: .7em 1.5em; 
  				outline: none; 
  				text-align: center;
			} 
			input.button:hover { background: rgb(232,95,76); } 
			input.button:active { background: rgb(152,15,0); }
			#content {
				position: fixed;
				top: 1%;
				left: 440px;				
			}
			.message {
				position: absolute;
				left: 42%;
				width: 1400px;
				height: 100px;
				word-wrap:break-word;
			}
			.solid-line-left {
    			position: fixed;
   				left: 340px;
   				top: 0px;
   				height: 100%;
   				width: 80px;
   				background: rgb(212,75,56);
   				box-shadow: 0 0 20px rgba(0,0,0,0.5);
    		}
    		#footer {
    			color: red;
    		}
    		.username {
    			color: blue;
    		}
		</style>
		<script type="text/javascript">
			function createXMLHttpRequest()
			{
				var xmlHttp;
				if(window.XMLHttpRequest) 
				{
					xmlHttp = new XMLHttpRequest();
				}
				else
				{
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlHttp.onreadystatechange = function() 
				{
					if (xmlHttp.readyState == 4 && xmlHttp.status == 200) 
					{
						document.getElementById('content').innerHTML = xmlHttp.responseText;
					}
				}
				xmlHttp.open("POST","messages.txt",true);
				xmlHttp.send();
			}
			createXMLHttpRequest();
			setInterval(createXMLHttpRequest,1000);
		</script>
	</head>
	<body>
	<?php
		if(isset($_SESSION['id'])) 
		{
			echo 
				'<div id="heading" class="text"><h3>You\'re loged in</h3></div>
				<form method="post" action="sign_in.php"> 
					<input class="button" name="logout" type="submit" value="Disconnect" /> 
				</form><br/>';
			echo
				'<form autocomplete="off" id="text-form" action="chat.php" type="post"> 
					<textarea class="chat" name="text" placeholder="Your message"></textarea><br/> 
					<input class="button" type="submit" value="Submit" /> 
					<input class="button" type="reset" value="Clear text" /> 
				</form>';
			echo 
				"<div id='content'></div>
				<div class='solid-line-left'></div>";
			if ($_SESSION['id'] == HOST_ID)
			{
				echo
					'Admin panel
					<form method="post" action="admin_config.php"> 
						<input class="button" name="reset" type="submit" value="Clear chat" /> 
					</form>';
			}
		}

		else 
		{
			echo 
				'<div id="heading" class="text"><h3>Connect to database</h3></div>
				<form autocomplete="off" method="post" action="sign_in.php"> 
					<input class="sign" name="username" placeholder="| Enter username" type="text"/><br/><br/> 
					<input class="sign" name="password" placeholder="| Enter password" type="password"/><br/><br/> 
					<input class="button" type="submit" value="Enter" /> 
				</form><br/>
				Do you have an account? If not, sign up <a href="register.php">here</a> for free!<br/>
				<div id="footer" class="text">'.$message.'</div>'; 
		}
	?>
	</body>
</html>