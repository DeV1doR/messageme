<?php
	require_once 'database_connection.php';

	function decoder($value,$salt=NULL,$connect_server) 
	{	
		$value = mysqli_real_escape_string($connect_server,trim($value));
		if (isset($salt)) {
			$dec_value = crypt($value,$salt);
			return $dec_value;
		} else {
			return $value;
		}
	}

	@$message = $_GET['message'];



	if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{

		if (empty($_POST['username']) or empty($_POST['password'])) 
		{
			$message = 'Login and password required';
			header('Location: register.php?message='.$message);
			exit();
		}

		$username = decoder($_POST['username'],NULL,$connect_server);
		$password = decoder($_POST['password'],$username,$connect_server);

		$query_check = sprintf("SELECT username FROM register WHERE username = '%s';",$username) 
			or die(mysqli_error($connect_server));
		$query_reg = sprintf("INSERT INTO register VALUES ('','%s','%s');",$username,$password)
			or die(mysqli_error($connect_server));

		$result_check = mysqli_query($connect_server,$query_check);	
		$row = mysqli_fetch_array($result_check, MYSQLI_ASSOC);

		if ($row['username'] == $username) 
		{
			$message = 'This user already exists';
			header('Location: register.php?message='.$message);
			exit();
		}

		$result_reg = mysqli_query($connect_server,$query_reg);
		$message = 'Registration success.';


	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<style type="text/css">
			.sign {
				height: 20px;
				width: 150px;
				resize: none;
				border-radius: 7px;
			}
			input.button {
				border-color: rgb(212,75,56);
				width: 150px;
  				color: #fff; 
  				text-decoration: none; 
  				user-select: none; 
  				background: rgb(212,75,56); 
  				padding: .7em 1.5em; 
  				outline: none; 
			} 
			input.button:hover { background: rgb(232,95,76); } 
			input.button:active { background: rgb(152,15,0); }
			#footer {
    			color: red;
    		}
		</style>
	</head>
	<body>
		<div id="heading" class="text"><h3>Simple registration!</h3></div>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			<input class="sign" name="username" placeholder="| Enter username" type="text"/><br/><br/>
			<input class="sign" name="password" placeholder="| Enter password" type="password"/><br/><br/>
			<input class="button" type="submit" value="Register" />
		</form><br/>
		Sign in <a href="sign_in.php">here</a><br/>
		<div id="footer" class="text"><?php echo $message; ?></div>
	</body>
</html>